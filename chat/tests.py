import datetime

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

from .models import Room
# Create your tests here.

def create_room(room_name):
    time = timezone.now()
    return Room.objects.create(room_name=room_name, pub_date=time)

class RoomModelsTests(TestCase):
    def test_one_room(self):
        create_room('room')
        response = self.client.get(reverse('chat:index'))
        self.assertQuerysetEqual(response.context['chat_room_list'], ['<Room: room>'])

    def test_two_rooms(self):
        create_room('room1')
        create_room('room2')
        response = self.client.get(reverse('chat:index'))
        self.assertQuerysetEqual(response.context['chat_room_list'], ['<Room: room2>', '<Room: room1>'])

class RoomResultViewTests(TestCase):
    def test_room(self):
        room = create_room('room')
        url = reverse('chat:detail', args=(room.id,))
        response = self.client.get(url)
        self.assertContains(response, room.room_name)
        

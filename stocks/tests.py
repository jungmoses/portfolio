from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils import timezone

from .models import Stock
# Create your tests here.

c = Client()


class StockModelsTest(TestCase):   
   def test_add_ticker(self):
      response = self.client.post(reverse('stocks:add'),{'ticker':'goog'})
      response_index = self.client.get(reverse('stocks:index'))
      self.assertQuerysetEqual(response_index.context['stock_list'], ['<Stock: GOOG>'])

   def test_register_user(self):
      response = self.client.post(reverse('stocks:user'),{'username':'zzz','password':'zzz'})
      self.assertQuerysetEqual(User.objects.all(), ['<User: zzz>'])

   def test_login(self):
      c.post(reverse('stocks:user'),{'username':'zzz','password':'zzz'})
      response = c.post(reverse('stocks:login'),{'username':'zzz','password':'zzz'})
      self.assertEqual(response.status_code, 302)

   def test_portfolio_not_login(self):
      response = self.client.get(reverse('stocks:portfolio'))
      self.assertEqual(response.status_code, 302)

   def test_portfolio_login(self):
      c.post(reverse('stocks:user'),{'username':'zzz','password':'zzz'})
      response = c.post(reverse('stocks:login'),{'username':'zzz','password':'zzz'})

      response = c.get(reverse('stocks:portfolio'))
      self.assertEqual(response.status_code, 200)


class StockPortfolioTest(TestCase):
   def test_portfolio_add(self):
      self.client.post(reverse('stocks:add'),{'ticker':'goog'})
      c.post(reverse('stocks:user'),{'username':'zzz','password':'zzz'})
      c.post(reverse('stocks:login'),{'username':'zzz','password':'zzz'})
      c.post(reverse('stocks:add_portfolio'),{'stock':'GOOG','id':1})
      #Stock.objects.get(pk=1).portfolio.add(User.objects.get(pk=1))
      response = c.get(reverse('stocks:portfolio'))
      #self.assertEqual(3,3)
      self.assertQuerysetEqual(response.context['stock_list'],['<Stock: GOOG>'])

class StockPortfolioTest2(TestCase):
   def test_portfolio_remove(self):
      self.client.post(reverse('stocks:add'),{'ticker':'goog'})
      self.client.post(reverse('stocks:add'),{'ticker':'goos'})
      c.post(reverse('stocks:user'),{'username':'zzz','password':'zzz'})
      c.post(reverse('stocks:login'),{'username':'zzz','password':'zzz'})
      c.post(reverse('stocks:add_portfolio'),{'stock':'GOOG','id':1})      
      c.post(reverse('stocks:add_portfolio'),{'stock':'GOOS','id':2})
      #Stock.objects.get(pk=1).portfolio.add(User.objects.get(pk=1))
      c.post(reverse('stocks:remove_portfolio'),{'stock':'GOOG','id':1})
      response = c.get(reverse('stocks:portfolio'))
      #self.assertEqual(3,3)
      self.assertQuerysetEqual(response.context['stock_list'],['<Stock: GOOS>'])

  

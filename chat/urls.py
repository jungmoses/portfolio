from django.urls import path

from . import views

app_name = 'chat'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:room_id>/message/', views.message, name='message'),
    path('create/', views.room_create, name='room'),
    ]

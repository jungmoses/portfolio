import json
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async
from django.utils import timezone, dateformat
from django.core.serializers.json import DjangoJSONEncoder

from .models import Message, Room

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        #print(self.scope['url_route'])
        #self.messages = await sync_to_async(self.get_messages)()
        #print(self.messages)
     
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    #def get_messages(self):
    #    return list(Message.objects.all())
        
    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    @sync_to_async
    def create_message(self, message, rm_num, username):
        room = Room.objects.get(pk=rm_num)
        if len(message.replace(' ', '')) > 0:
            m = Message(message_text=message, room=room, username=username)
            m.save()
    
    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        name = text_data_json['username']
        date = timezone.now()
        print(text_data_json)
        if len(name) == 0:
            name = 'Anonymous'
        #print(self.scope)
        if len(name.replace(' ', '')) == 0:
               name = 'Anonymous'
        await self.create_message(message=message, rm_num=self.scope['url_route']['kwargs']['room_name'],username=name)
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'username': name,
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        print(event)
        name = event['username']
        print(Message.objects)
        # Send message to WebSocket
        if len(message.replace(' ', '')) != 0:
            await self.send(text_data=json.dumps({
                'message': message,
                'username': name,
                'date': str(timezone.now())
            }))



from django.urls import path

from . import views

app_name = 'stocks'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('add/', views.add_ticker, name='add'),
    path('user/', views.create_user, name='user'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('portfolio/', views.PortfolioView.as_view(), name='portfolio'),
    path('addport/', views.add_portfolio, name='add_portfolio'),
    path('removeport/', views.remove_portfolio, name='remove_portfolio'),
    ]

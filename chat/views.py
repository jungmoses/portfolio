from django.shortcuts import render
from django.views import generic
from django.utils import timezone
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect

from .models import Room, Message

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'chat/index.html'
    context_object_name = 'chat_room_list'
    
    def get_queryset(self):
        return Room.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')

class DetailView(generic.DetailView):
    model = Room
    template_name = 'chat/detail.html'
    def get_queryset(self):
        return Room.objects

def room_create(request):
    rm = request.POST['room']
    if len(rm.replace(' ', '')) == 0:
        return HttpResponseRedirect(reverse('chat:index'))
    room = Room(room_name=request.POST['room'], pub_date=timezone.now())
    room.save()
    return HttpResponseRedirect(reverse('chat:index'))

def message(request, room_id):
    room = get_object_or_404(Room, pk=room_id)
    if len(request.POST['message'].replace(' ','')) == 0:
        return HttpResponseRedirect(reverse('chat:detail',args=(room_id,)))
    m = Message(message_text=request.POST['message'], username=request.POST['username'], room=room, pub_date=timezone.now())
    m.save()
    #print(request.POST)
    return HttpResponseRedirect(reverse('chat:detail', args=(room_id,)))

from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Stock(models.Model):
    symbol = models.CharField(max_length=10, unique=True)
    longName = models.CharField(max_length=100, default=None, blank=True, null=True)
    sector = models.CharField(max_length=100, default=None, blank=True, null=True)
    industry = models.CharField(max_length=100, default=None, blank=True, null=True)
    country = models.CharField(max_length=100, default=None, blank=True, null=True)
    currency = models.CharField(max_length=100, default=None, blank=True, null=True)
    added = models.DateTimeField()
    # users added stock
    portfolio = models.ManyToManyField(User)
    
    def __str__(self):
        return self.symbol

dj-database-url==0.5.0
Django==3.1.2
gunicorn==20.0.4
psycopg2-binary==2.8.6
whitenoise==5.2.0
yfinance
django-heroku
channels
channels_redis
redis
daphne==3.0.1

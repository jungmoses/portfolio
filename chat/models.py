import datetime

from django.db import models
from django.utils import timezone

# Create your models here.
class Room(models.Model):
    room_name = models.CharField(max_length=100, null=False, blank=False)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.room_name
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.boolean = True

class Message(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    username = models.CharField(max_length=30, default='Anonymous')
    message_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    pub_date = models.DateTimeField('date published', default=timezone.now)
    def __str__(self):
        return self.message_text

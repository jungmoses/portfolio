from django.contrib import admin

from .models import Room, Message

# Register your models here.
class RoomAdmin(admin.ModelAdmin):
    model = Room
    
class MessageAdmin(admin.ModelAdmin):
    model = Message
    
admin.site.register(Room, RoomAdmin)
admin.site.register(Message, MessageAdmin)

from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.contrib.messages import get_messages
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.mixins import LoginRequiredMixin

from asgiref.sync import sync_to_async
import asyncio

from .models import Stock

import yfinance as yf

# Create your views here.

class DetailView(generic.DetailView):
    template_name = 'stocks/stockinfo.html'
    model = Stock

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c = Stock.objects.get(pk=self.kwargs['pk'])
        ticker = yf.Ticker(c.symbol)
        info = ticker.info
        prices = ticker.history(period='1mo')[['Close']]
        context['prices'] = prices.values.tolist()
        dates = ticker.history(period='1mo').index.tolist()
        datess = [int(x.strftime("%s%f"))/1000 for x in dates]
        context['dates'] = datess
        context['info'] = info
        if c.portfolio.filter(username=self.request.user).exists():
            context['userhas'] = True
        elif self.request.user.is_authenticated:
            context['userhas'] = False
        else:
            context['userhas'] = None            
        #print(prices)
        return context   
   
    def get_queryset(self):
        return Stock.objects

class IndexView(generic.ListView):
    template_name = 'stocks/index.html'
    context_object_name = 'stock_list'
    paginate_by = 7

    def get_messages(self):
        storage = get_messages(self.request)
        return storage

    '''
    def get_yf(self):
        list = []
        for s in Stock.objects.all():
            list.append(yf.Ticker(str(s)))
        return list
    '''
    
    def get_queryset(self):
        #l = self.get_yf()
        return Stock.objects.all().order_by('-added')

class PortfolioView(LoginRequiredMixin, generic.ListView):
    template_name = 'stocks/index.html'
    context_object_name = 'stock_list'
    paginate_by = 7

    def get_messages(self):
        storage = get_messages(self.request)
        return storage
    
    def get_queryset(self):
        return Stock.objects.filter(portfolio=self.request.user)
    
    
@sync_to_async
def get_ticker(symbol):
    y = yf.Ticker(symbol)
    #new_ticker=Stock(symbol='hi')    
    if 'sector' in y.info:
        #print(y.info)
        try:
            new_ticker = Stock(symbol=symbol.upper(), longName=y.info['longName'], sector=y.info['sector'], industry=y.info['industry'], country=y.info['country'], currency=y.info['currency'], added=timezone.now())
            new_ticker.save()
            print('saved')
            return (yf.Ticker(symbol), True)
        except Exception as e:
            a =Stock.objects.get(symbol=symbol)
            a.added = timezone.now()
            a.save()
            

            return (yf.Ticker(symbol), False)
    else:
        return (yf.Ticker(symbol), False)

async def add_ticker(request):
    ticker = request.POST['ticker'].upper()
    s = await( asyncio.create_task(get_ticker(ticker)))
    # 'sector' in ticker
    if 'sector' in s[0].info:
        if s[1] == False:            
            messages.info(request, ticker.upper() + ' is already in the list.')
            return HttpResponseRedirect(reverse('stocks:index',))
        else:
             messages.success(request, ticker.upper() + ' was added.')
             return HttpResponseRedirect(reverse('stocks:index')
    )
    messages.info(request, ticker.upper() + ' does not exist.')
    return HttpResponseRedirect(reverse('stocks:index'))
    
    ticker = request.POST['ticker'].upper()
    s = await asyncio.create_task(get_ticker(ticker))
    # 'sector' in ticker
    if 'sector' in s[0].info:
        if s[1] == False:
            
            messages.info(request, ticker.upper() + ' is already in the list.')
        return HttpResponseRedirect(reverse('stocks:index',))
    else:
        messages.info(request, ticker.upper() + ' does not exist.')
        return HttpResponseRedirect(reverse('stocks:index'))
    messages.success(request, ticker.upper() + ' was added.')
    return HttpResponseRedirect(reverse('stocks:index')
    )

def create_user(request):
    try:
        user = User.objects.create_user(username=request.POST['username'], password=request.POST['password'])        
        messages.info(request, 'Your account has been created')
        return HttpResponseRedirect(reverse('stocks:index'))
    except:
        messages.error(request, 'Try a different username')
        return HttpResponseRedirect(reverse('stocks:index'))

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        auth_login(request, user)        
        return HttpResponseRedirect(reverse('stocks:index'))
    else:
        messages.error(request, 'Account not found')
        return HttpResponseRedirect(reverse('stocks:index'))

def logout(request):
    messages.info(request, "Logged out")
    print('hi')
    auth_logout(request)
    return HttpResponseRedirect(reverse('stocks:index'))

def add_portfolio(request):
    id = request.POST['id']
    try:
        s = Stock.objects.get(pk=id)
        s.portfolio.add(request.user)
        messages.success(request, 'Added to portfolio')
        return HttpResponseRedirect(reverse('stocks:detail', args=[id]))
    except Exception as e:
        print(e)
        messages.info(request, 'Already added to portfolio')
        return HttpResponseRedirect(reverse('stocks:detail', args=[id]))

def remove_portfolio(request):
    id = request.POST['id']
    try:
        s = Stock.objects.get(pk=id)
        s.portfolio.remove(request.user)
        messages.success(request, 'Removed from portfolio')
        return HttpResponseRedirect(reverse('stocks:detail', args=[id]))
    except Exception as e:
        print(e)
        messages.info(request, 'Already removed from portfolio')
        return HttpResponseRedirect(reverse('stocks:detail', args=[id]))
